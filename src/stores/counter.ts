import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useplusCounterStore = defineStore("counter", () => {
  const count = ref(0);
  const plusCount = computed(() => count.value * 2);

  function increment() {
    count.value++;
  }

  function decrease() {
    if (count.value != 0) {
      count.value--;
    }
  }

  return { count, plusCount, increment, decrease };
});

// // export const useCounterStore = defineStore("counter", () => {
//   const count = ref(0);
//   const doubleCount = computed(() => count.value * 2);
//   function increment() {
//     count.value++;
//   }
//   function decrease() {
//     if (count.value != 0) {
//       count.value--;
//     }
//   }

//   return { count, doubleCount, increment, decrease };
// });
